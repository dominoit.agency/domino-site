// Libs
import { useMemo } from 'react'
import {
  ApolloClient,
  createHttpLink,
  from,
  InMemoryCache,
  NormalizedCacheObject,
} from '@apollo/client'
import { setContext } from '@apollo/client/link/context'
import { onError } from '@apollo/client/link/error'
import merge from 'deepmerge'
import isEqual from 'lodash/isEqual'
// Hooks
import { useClientLocalStore, useToast } from '@hooks'

// Apollo client
let apolloClientConnect: ApolloClient<NormalizedCacheObject> | undefined

// Apollo error link
const errorLink = onError(({ graphQLErrors, networkError }) => {
  // Msg
  const { customToast } = useToast()
  // GraphQL error
  if (graphQLErrors) graphQLErrors.forEach(({ message }) => customToast(message, 'error'))
  // Network error
  if (networkError) customToast(networkError.message, 'error')
})
// BE link
const httpLinkBE = createHttpLink({
  uri: `${process.env.SITE_URL}/api/graphql`,
  credentials: 'same-origin',
})
// FE link
const httpLinkFE = createHttpLink({
  uri: `/api/graphql`,
  credentials: 'same-origin',
})
// Headers
const authLink = setContext((_, { headers }) => {
  // Store
  const { clientLocalStore } = useClientLocalStore()
  // Token
  const token = clientLocalStore({ params: 'get', name: 'token' }) || null
  // Locale
  const locale = document.querySelector('meta[name="locale"]')?.getAttribute('content')

  // Return
  return {
    headers: {
      ...headers,
      authorization: token,
      locale,
    },
  }
})

// Create isomorph link
function createIsomorphLink() {
  if (typeof window === 'undefined') {
    return httpLinkBE
  } else {
    return authLink.concat(httpLinkFE)
  }
}

// Create apollo client
function createApolloClient() {
  return new ApolloClient({
    ssrMode: typeof window === 'undefined',
    link: from([errorLink, createIsomorphLink()]),
    cache: new InMemoryCache(),
    ssrForceFetchDelay: 100,
    defaultOptions: {
      watchQuery: {
        fetchPolicy: 'cache-and-network',
        nextFetchPolicy: 'cache-first',
        refetchWritePolicy: 'merge',
      },
      mutate: {
        awaitRefetchQueries: true,
      },
    },
  })
}

// Initialize apollo
export function initializeApollo(initialState = null) {
  const _apolloClient = apolloClientConnect ?? createApolloClient()
  // If your page has Next.js data fetching methods that use Apollo Client, the initial state
  // Gets hydrated here
  if (initialState) {
    // Get existing cache, loaded during client side data fetching
    const existingCache = _apolloClient.extract()
    // Merge the initialState from getStaticProps/getServerSideProps in the existing cache
    const data = merge(existingCache, initialState, {
      // Combine arrays using object equality (like in sets)
      arrayMerge: (destinationArray, sourceArray) => [
        ...sourceArray,
        ...destinationArray.filter((d) => sourceArray.every((s) => !isEqual(d, s))),
      ],
    })
    // Restore the cache with the merged data
    _apolloClient.cache.restore(data)
  }
  // For SSG and SSR always create a new Apollo Client
  if (typeof window === 'undefined') return _apolloClient
  // Create the Apollo Client once in the client
  if (!apolloClientConnect) apolloClientConnect = _apolloClient

  // Return
  return _apolloClient
}

// Add apollo state only SSR and SSG
export function addApolloState(
  client: { cache: { extract: () => any } },
  pageProps: { props: any; revalidate?: number },
) {
  // Apollo state props
  if (pageProps?.props) pageProps.props['__APOLLO_STATE__'] = client.cache.extract()

  // Return
  return pageProps
}

// Apollo hook
export function useApollo(pageProps: { [x: string]: any }) {
  // State
  const state = pageProps['__APOLLO_STATE__']

  // Return
  return useMemo(() => initializeApollo(state), [state])
}
