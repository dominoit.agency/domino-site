import Document, { Html, Head, Main, NextScript } from 'next/document'

class MyDocument extends Document {
  render() {
    return (
      <Html>
        <Head />
        <body>
          <script
            dangerouslySetInnerHTML={{
              __html: themeInitializerScript,
            }}
          ></script>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

// This function needs to be a String
const themeInitializerScript = `(function() {
	${setInitialColorMode.toString()}
	setInitialColorMode();
})()
`

function setInitialColorMode() {
  // Check initial color preference
  function getInitialColorMode() {
    const persistedPreferenceMode = window.localStorage.getItem('theme')

    if (persistedPreferenceMode) {
      return persistedPreferenceMode
    }

    // Check the current preference
    const preference = window.matchMedia('(prefers-color-scheme: dark)')

    if (preference?.matches) {
      return preference.matches ? 'dark' : 'light'
    }

    return 'light'
  }

  const currentColorMode = getInitialColorMode()

  // If darkmode apply darkmode
  if (currentColorMode === 'dark') document.body.setAttribute('data-theme', 'dark')
}

export default MyDocument
