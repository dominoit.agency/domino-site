// Libs
import '@styles/globals.scss'
import type { AppProps } from 'next/app'
import { useEffect } from 'react'
import { AppLayout } from '@components/templates'
// Apollo
import { ApolloProvider } from '@apollo/client'
import { useApollo } from '@graphql/connect'
// Context
import { ContextProvider } from '@store/context'
// Seo and form provider
import { FormSubmitProvider, Seo } from '@components/elements'
// Toastify
import { ToastContainer } from '@hooks'
// Progress bar
import { NextProgress } from '@components/elements'

// App
function MyApp({ Component, pageProps, router }: AppProps) {
  // Apollo client
  const apolloClient = useApollo(pageProps)
  // Watch from scroll
  useEffect(() => {
    window.scroll(0, 0)
  }, [router.route])

  // Return
  return (
    <>
      <Seo locale={router.locale || router.defaultLocale} />

      <ApolloProvider client={apolloClient}>
        <ContextProvider>
          <FormSubmitProvider>
            <AppLayout>
              <Component {...pageProps} />
            </AppLayout>
          </FormSubmitProvider>
        </ContextProvider>
      </ApolloProvider>

      <ToastContainer />

      <NextProgress />
    </>
  )
}

export default MyApp
