// Libs
import React, { FC, memo } from 'react'
import styles from './header.module.scss'
import container from '@styles/modules/container.module.scss'
import { ButtonBurgerMenu, Logo } from '@components/ui'
import { LangSelector, ThemeSelector } from '@components/templates'
import { useAppContext } from '@store/context'
import { AnimatePresence, domAnimation, LazyMotion, m } from 'framer-motion'
import { appearanceAnim } from '@constants/framer-motion'

// Component
const Header: FC = () => {
  // Context
  const { burgerMenu, setBurgerMenu } = useAppContext()

  // Return
  return (
    <LazyMotion features={domAnimation}>
      <header>
        <div className={`${container.container_outer} ${styles.header}`}>
          <m.div layout className={styles.header__content} id={'header'}>
            <Logo />

            <div />

            <div />

            <AnimatePresence>
              {!burgerMenu && (
                <>
                  <m.div
                    variants={appearanceAnim.center}
                    initial={'initial'}
                    animate={appearanceAnim.center.animate(1)}
                    exit={'exit'}
                    className={styles.header__theme}
                    key={'theme'}
                  >
                    <ThemeSelector />
                  </m.div>

                  <m.div
                    variants={appearanceAnim.center}
                    initial={'initial'}
                    animate={'animate'}
                    exit={appearanceAnim.center.exit(1)}
                    className={styles.header__lang}
                    key={'lang'}
                  >
                    <LangSelector />
                  </m.div>
                </>
              )}
            </AnimatePresence>

            <ButtonBurgerMenu active={burgerMenu} onChange={() => setBurgerMenu(!burgerMenu)} />
          </m.div>
        </div>
      </header>
    </LazyMotion>
  )
}

export default memo(Header)
