// Libs
import React, { FC, memo } from 'react'
import styles from './lang-selector.module.scss'
import { useApolloClient } from '@apollo/client'
import { useRouter } from 'next/router'
import { usePopup } from '@hooks'
import { AnimatePresence, domAnimation, LazyMotion, m } from 'framer-motion'

// Interface
interface ILangSelector {
  variant?: 'hidden' | 'visible'
}

// Component
const LangSelector: FC<ILangSelector> = ({ variant = 'hidden' }) => {
  // Apollo cache
  const apolloClient = useApolloClient()
  // Router
  const { locales, locale, defaultLocale, push, pathname, asPath } = useRouter()
  // Popup control
  const { popup, popupRef, handlePopup } = usePopup()
  // On change
  const handleOnChange = (value: string) => {
    push(pathname, asPath, {
      locale: value === 'en' ? 'en' : value === 'pl' ? 'pl' : defaultLocale,
    }).finally(() => apolloClient.resetStore())
  }

  // Return
  return (
    <LazyMotion features={domAnimation}>
      <div className={styles.lang_selector} ref={popupRef} onClick={handlePopup}>
        <p
          className={`${styles.lang_selector__item} ${
            (variant !== 'visible' ? popup : true) ? styles.active : ''
          }`}
        >
          {locale}
        </p>

        <AnimatePresence>
          {(variant !== 'visible' ? popup : true) && (
            <m.div
              className={styles.lang_selector__list}
              variants={anim_lang}
              initial={'initial'}
              animate={'animate'}
              exit={anim_lang.exit(locales?.length || 1)}
            >
              {locales
                ?.filter((lang) => lang !== locale)
                .map((el, i) => (
                  <React.Fragment key={i}>
                    <m.p
                      className={`${styles.lang_selector__item} ${
                        el === locale ? styles.active : ''
                      }`}
                      onClick={() => handleOnChange(el)}
                      variants={anim_selector}
                      custom={i + 1}
                      initial={'initial'}
                      animate={'animate'}
                      exit={'exit'}
                    >
                      {el}
                    </m.p>
                  </React.Fragment>
                ))}
            </m.div>
          )}
        </AnimatePresence>
      </div>
    </LazyMotion>
  )
}

export default memo(LangSelector)

// Component animate
const anim_selector = {
  initial: {
    y: '-120%',
  },

  animate: (custom = 0) => ({
    y: 0,
    transition: {
      delay: custom * 0.1,
      default: { duration: 0.2 },
    },
  }),

  exit: (custom = 0) => ({
    y: '-120%',
    transition: {
      delay: custom * 0.1,
      default: { duration: 0.2 },
    },
  }),
}
const anim_lang = {
  initial: {
    overflow: 'hidden',
    width: '0',
  },

  animate: (custom = 0) => ({
    overflow: 'hidden',
    width: 'auto',
    transition: {
      delay: custom * 0.1,
      default: { duration: 0.2 },
    },
  }),

  exit: (custom = 0) => ({
    overflow: 'hidden',
    width: '0',
    transition: {
      delay: custom * 0.1,
      default: { duration: 0.2 },
    },
  }),
}
