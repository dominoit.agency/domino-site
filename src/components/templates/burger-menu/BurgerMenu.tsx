// Libs
import { FC, memo } from 'react'
import styles from './burger-menu.module.scss'
import Router from 'next/router'
import { LangSelector, NavLink } from '@components/templates'
import { AnimatePresence, domAnimation, LazyMotion, m } from 'framer-motion'

// Interface
interface IBurgerMenu {
  active: boolean
  onChange: (e: boolean) => void
}

// Component
const BurgerMenu: FC<IBurgerMenu> = ({ active = false, onChange }) => {
  // Change active
  const handleOnChange = () => onChange(!active)
  // Watch from path
  Router.events.on('routeChangeStart', () => active && onChange(!active))

  // Return
  return (
    <LazyMotion features={domAnimation}>
      <AnimatePresence>
        {active && (
          <m.div
            className={styles.burger_menu}
            onClick={handleOnChange}
            variants={anim_burger_menu}
            initial={'initial'}
            animate={'animate'}
            exit={'exit'}
          >
            <div className={styles.burger_menu__content}>
              <NavLink variant={'burger-menu'} />

              <LangSelector variant={'visible'} />
            </div>
          </m.div>
        )}
      </AnimatePresence>
    </LazyMotion>
  )
}

export default memo(BurgerMenu)

// Component animate
const anim_burger_menu = {
  initial: {
    y: '-120%',
  },
  animate: {
    y: '0',
    transition: { default: { duration: 0.25, type: 'spring', stiffness: 40, dump: 200 } },
  },
  exit: {
    y: '-120%',
    transition: { default: { duration: 0.25, type: 'spring', stiffness: 40, dump: 200 } },
  },
}
