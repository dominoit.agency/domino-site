// Libs
import React, { FC, memo, useEffect, useState } from 'react'
import styles from './theme-selector.module.scss'
import { useClientLocalStore } from '@hooks'
import { AnimatePresence, domAnimation, LazyMotion, m } from 'framer-motion'

// Component
const ThemeSelector: FC = () => {
  // Client local store
  const { clientLocalStore } = useClientLocalStore()
  // Theme
  const [theme, setTheme] = useState<string>('dark')
  // On change
  const handleOnChange = () => {
    document.body.setAttribute('data-theme', theme === 'dark' ? 'light' : 'dark')
    setTheme(theme === 'dark' ? 'light' : 'dark')
    clientLocalStore({ params: 'set', name: 'theme', data: theme === 'dark' ? 'light' : 'dark' })
  }
  // Watch from theme
  useEffect(() => {
    // In client local store
    const themeType = clientLocalStore({ params: 'get', name: 'theme' })
    // Device theme default
    const deviceThemeDefault = document.body.getAttribute('data-theme') || 'light'
    // Set theme
    themeType ? setTheme(themeType) : setTheme(deviceThemeDefault)
  }, [])

  // Return
  return (
    <LazyMotion features={domAnimation}>
      <div className={styles.theme_selector} onClick={handleOnChange}>
        <AnimatePresence mode={'wait'} initial={false}>
          {theme === 'dark' ? (
            <m.div
              key={'dark'}
              variants={anim_theme}
              initial={'initial'}
              animate={'animate'}
              exit={'exit'}
            >
              <svg
                width='30'
                height='30'
                viewBox='0 0 30 30'
                fill='none'
                xmlns='http://www.w3.org/2000/svg'
              >
                <g opacity='0.5' clipPath='url(#clip0_1101_122)'>
                  <path
                    d='M15 26.25V28.75'
                    stroke='white'
                    strokeWidth='2'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                  />
                  <path
                    d='M22.9502 22.95L24.7252 24.725'
                    stroke='white'
                    strokeWidth='2'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                  />
                  <path
                    d='M5.27539 24.725L7.05039 22.95'
                    stroke='white'
                    strokeWidth='2'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                  />
                  <path
                    d='M26.25 15H28.75'
                    stroke='white'
                    strokeWidth='2'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                  />
                  <path
                    d='M1.25 15H3.75'
                    stroke='white'
                    strokeWidth='2'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                  />
                  <path
                    d='M15 21.25C18.4518 21.25 21.25 18.4518 21.25 15C21.25 11.5482 18.4518 8.75 15 8.75C11.5482 8.75 8.75 11.5482 8.75 15C8.75 18.4518 11.5482 21.25 15 21.25Z'
                    stroke='white'
                    strokeWidth='2'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                  />
                  <path
                    d='M22.9502 7.0499L24.7252 5.2749'
                    stroke='white'
                    strokeWidth='2'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                  />
                  <path
                    d='M5.27539 5.2749L7.05039 7.0499'
                    stroke='white'
                    strokeWidth='2'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                  />
                  <path
                    d='M15 1.25V3.75'
                    stroke='white'
                    strokeWidth='2'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                  />
                </g>
                <defs>
                  <clipPath id='clip0_1101_122'>
                    <rect width='30' height='30' fill='white' />
                  </clipPath>
                </defs>
              </svg>
            </m.div>
          ) : (
            <m.div
              key={'light'}
              variants={anim_theme}
              initial={'initial'}
              animate={'animate'}
              exit={'exit'}
            >
              <svg
                width='26'
                height='26'
                viewBox='0 0 26 26'
                fill='none'
                xmlns='http://www.w3.org/2000/svg'
                stroke='#242424'
              >
                <g opacity='0.5'>
                  <path
                    d='M12.1052 13.8948C15.4539 17.2434 20.4749 18.0537 24.6333 16.1364C23.1999 21.4137 18.3019 25.1676 12.6583 24.9942C6.3054 24.799 1.20098 19.6946 1.00578 13.3417C0.832369 7.69815 4.58628 2.80012 9.86357 1.36667C7.94634 5.52513 8.75656 10.5461 12.1052 13.8948Z'
                    strokeWidth='2'
                    strokeLinecap='round'
                    strokeLinejoin='round'
                  />
                </g>
              </svg>
            </m.div>
          )}
        </AnimatePresence>
      </div>
    </LazyMotion>
  )
}

export default memo(ThemeSelector)

// Component animate
const anim_theme = {
  initial: {
    y: '-120%',
  },

  animate: (custom = 0) => ({
    y: 0,
    transition: {
      delay: custom * 0.1,
      default: { duration: 0.15 },
    },
  }),

  exit: (custom = 0) => ({
    y: '-120%',
    transition: {
      delay: custom * 0.1,
      default: { duration: 0.15 },
    },
  }),
}
