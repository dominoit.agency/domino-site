// Libs
import React, { FC, memo } from 'react'
import styles from './footer.module.scss'
import container from '@styles/modules/container.module.scss'
import { NavLink } from '@components/templates'
import { Logo } from '@components/ui'

// Component
const Footer: FC = () => {
  // Return
  return (
    <footer className={styles.footer}>
      <div className={container.container_outer}>
        <div className={styles.footer__content}>
          <NavLink variant={'column'} />

          <Logo />
        </div>
      </div>
    </footer>
  )
}

export default memo(Footer)
