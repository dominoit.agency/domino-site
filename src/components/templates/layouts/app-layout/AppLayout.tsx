// Libs
import React, { FC, memo, ReactNode } from 'react'
import styles from './app-layout.module.scss'
import { BurgerMenu, Footer, Header } from '@components/templates'
import { ButtonScrollToTop } from '@components/ui'
import { useRouter } from 'next/router'
import { useAppContext } from '@store/context'

// Component
const AppLayout: FC<{ children: ReactNode }> = ({ children }) => {
  // Router
  const { route } = useRouter()
  // Context
  const { burgerMenu, setBurgerMenu } = useAppContext()

  // Return
  return (
    <>
      {route !== '/login' && route !== '/registration' && route !== '/404' ? (
        <>
          <Header />

          <main className={styles.app_layout}>{children}</main>

          <Footer />

          <BurgerMenu active={burgerMenu} onChange={(e) => setBurgerMenu(e)} />
        </>
      ) : (
        { children }
      )}

      <ButtonScrollToTop />
    </>
  )
}

export default memo(AppLayout)
