// Libs
import { FC, memo } from 'react'
import styles from './nav-link.module.scss'
import { useRouter } from 'next/router'
import { linksPublic } from '@constants/locale/components'

// Interface
interface INavLink {
  variant?: 'row' | 'column' | 'burger-menu'
}

// Component
const NavLink: FC<INavLink> = ({ variant = 'row' }) => {
  // Router
  const { push, locale, defaultLocale, pathname } = useRouter()
  // Locale
  const lang = locale || defaultLocale || ''

  // Return
  return (
    <div
      className={`${styles.nav_link} ${variant === 'column' ? styles.column : ''} ${
        variant === 'burger-menu' ? styles.burger_menu : ''
      }`}
    >
      {linksPublic &&
        linksPublic[lang].map((item, i) => (
          <div
            key={`${item} ${i}`}
            className={`${styles.nav_link__item} ${pathname === item.href ? styles.active : ''}`}
            onClick={() => push(item.href)}
          >
            {item.name}
          </div>
        ))}
    </div>
  )
}

export default memo(NavLink)
