// Libs
import React, { ChangeEvent, FC, forwardRef, memo, Ref, useEffect, useRef, useState } from 'react'
import styles from './selector.module.scss'
import { AnimatePresence, domAnimation, LazyMotion, m } from 'framer-motion'

// Interface
interface ISelector {
  title?: string
  onChange?: (value: string | number) => void
  value?: string | number
  defaultValue?: string
  search?: boolean
  errors?: string
  position?: 'relative' | 'absolute'
  popupItems: { value: string | number; name: string }[]
  classNameSelector?: string
  classNamePopup?: string
}

// Component
const Selector: FC<ISelector> = forwardRef(
  (
    {
      title,
      onChange,
      value,
      defaultValue,
      search = false,
      errors,
      position = 'relative',
      popupItems,
      classNameSelector,
      classNamePopup,
    },
    ref: Ref<HTMLInputElement>,
  ) => {
    // Search input
    const [searchInput, setSearchInput] = useState<string>('')
    // Ref
    const selectorRef = useRef<HTMLDivElement>(null)
    // Popup
    const [popup, setPopup] = useState(false)
    // Popup active
    const handlePopupClick = () => setPopup(!popup)
    const handleOutPopupClick = (e: any) => {
      const path = e.path || (e.composedPath() && e.composedPath())
      if (!path.includes(selectorRef.current)) setPopup(false)
    }
    // Watch from outside click
    useEffect(() => {
      document.querySelector('html')?.addEventListener('click', handleOutPopupClick)
      return () => removeEventListener('click', handleOutPopupClick)
    }, [])
    // On select item
    const handleOnSelectItem = (val: string | number) => {
      searchInput && setSearchInput('')
      onChange && onChange(val)
    }
    // Search in items
    const handleOnSearch = (e: ChangeEvent<HTMLInputElement>) => setSearchInput(e.target.value)

    // Return
    return (
      <LazyMotion features={domAnimation}>
        <label className={styles.wrapper}>
          {title && <div className={styles.wrapper__label}>{title}</div>}

          <div
            className={`${styles.wrapper__selector_body} ${classNameSelector} ${
              popup ? styles.active : ''
            }`}
            ref={selectorRef}
            onClick={handlePopupClick}
          >
            <input
              ref={ref}
              className={styles.wrapper__selector_input}
              placeholder={
                value ? popupItems.filter((el) => el.value === value)[0]?.name : defaultValue
              }
              value={searchInput}
              readOnly={popup ? !search : !popup}
              autoComplete={'off'}
              style={{ cursor: !search ? 'pointer' : 'text' }}
              onChange={handleOnSearch}
            />

            <div
              className={styles.wrapper__selector_svg}
              onClick={(event) => event.stopPropagation()}
            >
              <svg
                className={`${popup ? styles.active : ''}`}
                viewBox='0 0 24 24'
                fill='none'
                stroke='black'
                xmlns='http://www.w3.org/2000/svg'
              >
                <path
                  d='M15 4L7 12L15 20'
                  strokeWidth='1.5'
                  strokeLinecap='round'
                  strokeLinejoin='round'
                />
              </svg>
            </div>
          </div>

          <AnimatePresence>
            {errors && (
              <m.div variants={anim_error} initial={'initial'} animate={'animate'} exit={'exit'}>
                <p className={styles.error}>{errors}</p>
              </m.div>
            )}
          </AnimatePresence>

          <AnimatePresence>
            {popup && (
              <m.div
                className={`${styles.wrapper__popup_body} ${classNamePopup} ${
                  position === 'absolute' ? styles.absolute : ''
                }`}
                variants={anim_popup}
                initial={'initial'}
                animate={'animate'}
                exit={'exit'}
              >
                <m.div
                  className={styles.wrapper__popup}
                  onClick={(e) => e.stopPropagation()}
                  variants={anim_popup_scroll}
                  initial={'initial'}
                  animate={'animate'}
                  exit={'exit'}
                >
                  <ul>
                    {popupItems &&
                      (!searchInput
                        ? popupItems
                        : popupItems.filter((el) => el.name.toLowerCase().includes(searchInput))
                      ).map((el, i) => (
                        <li
                          key={`SelectorItem${el.value}${i}`}
                          className={el.value === value ? styles.disable : ''}
                          onClick={() => handleOnSelectItem(el.value)}
                        >
                          {el.name}
                        </li>
                      ))}
                  </ul>
                </m.div>
              </m.div>
            )}
          </AnimatePresence>
        </label>
      </LazyMotion>
    )
  },
)

Selector.displayName = 'Selector'
export default memo(Selector)

// Component animate
const anim_error = {
  initial: {
    opacity: 0,
    height: '0',
  },
  animate: {
    opacity: 1,
    height: 'auto',
    transition: { default: { duration: 0.25 } },
  },
  exit: {
    opacity: 0,
    height: '0',
    transition: { default: { duration: 0.25 } },
  },
}
const anim_popup = {
  initial: { height: '0' },
  animate: {
    height: 'auto',
    transition: {
      default: { duration: 0.25 },
    },
  },
  exit: {
    height: '0',
    transition: {
      default: { duration: 0.25 },
    },
  },
}
const anim_popup_scroll = {
  initial: { overflow: 'hidden' },
  animate: {
    overflow: 'auto',
    transition: {
      delay: 0.3,
    },
  },
  exit: { overflow: 'hidden' },
}
