// Libs
import React, { FC, memo } from 'react'
import styles from './pagePreloader.module.scss'
import { AnimatePresence, domAnimation, LazyMotion, m } from 'framer-motion'

// Interface
interface IPreloader {
  active: boolean
  position?: 'fixed' | 'absolute'
}

// Component
const PagePreloader: FC<IPreloader> = ({ active = false, position = 'fixed' }) => (
  <LazyMotion features={domAnimation}>
    <AnimatePresence>
      {active && (
        <m.div
          className={`${styles.pagePreloader} ${position === 'absolute' && styles.absolute}`}
          variants={anim_loader}
          initial={'initial'}
          animate={'animate'}
          exit={'exit'}
        >
          <div className={styles.pagePreloader__loader} />
        </m.div>
      )}
    </AnimatePresence>
  </LazyMotion>
)

export default memo(PagePreloader)

// Component animate
const anim_loader = {
  initial: {
    opacity: 0,
  },
  animate: {
    opacity: 1,
    transition: { default: { duration: 0.25 } },
  },
  exit: {
    opacity: 0,
    transition: { default: { duration: 0.25 } },
  },
}
