// Libs
import React, { CSSProperties, FC, memo } from 'react'
import ContentLoader from 'react-content-loader'

// Component
const ItemPreloader: FC<{ w?: string; h?: string; style?: CSSProperties }> = ({
  w = '100%',
  h = '28px',
  style,
}) => {
  // Return
  return (
    <ContentLoader
      speed={2}
      width={w}
      height={h}
      backgroundColor={'rgba(255,255,255,0)'}
      foregroundColor={'rgba(255,255,255,0.55)'}
      style={style}
    >
      <rect x='0' y='0' rx='0' ry='0' width={w} height={h} />
    </ContentLoader>
  )
}

export default memo(ItemPreloader)
