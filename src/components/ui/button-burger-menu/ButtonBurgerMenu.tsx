// Libs
import { FC, memo, useEffect, useState } from 'react'
import styles from './button-burger-menu.module.scss'
import { AnimatePresence, domAnimation, LazyMotion, m } from 'framer-motion'

// Interface
interface IButtonBurgerMenu {
  active: boolean
  onChange: () => void
}

// Component
const ButtonBurgerMenu: FC<IButtonBurgerMenu> = ({ active = false, onChange }) => {
  // Prev params
  const [prevParams, setPrevParams] = useState<string | null>(null)
  // Device type
  const getDeviceType = () => {
    const ua = navigator.userAgent
    if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
      return 'tablet'
    }
    if (
      /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
        ua,
      )
    ) {
      return 'mobile'
    }
    return 'desktop'
  }
  // Watch from params
  useEffect(() => {
    const body = document.querySelector('html')

    !prevParams && body && setPrevParams(window.getComputedStyle(body).overflow)

    !active && setPrevParams(null)
  }, [active])
  // Control scroll
  useEffect(() => {
    const body = document.querySelector('body')
    const header = document.querySelector('header')

    if (body) {
      if (active) {
        body.style.overflow = 'hidden'
        if (getDeviceType() === 'desktop') body.style.paddingRight = '12px'
        if (getDeviceType() === 'desktop' && header) header.style.paddingRight = '12px'
      } else if (!active && body) {
        body.style.overflow = prevParams || ''
        if (getDeviceType() === 'desktop') body.style.paddingRight = '0'
        if (getDeviceType() === 'desktop' && header) header.style.paddingRight = '0'
      }
    }
  }, [active, prevParams])

  // Return
  return (
    <LazyMotion features={domAnimation}>
      <div className={styles.button_burger_menu} onClick={onChange}>
        <div className={styles.button_burger_menu__svg}>
          <svg viewBox='0 0 28 28' fill='white' xmlns='http://www.w3.org/2000/svg'>
            <circle cx='5' cy='5' r='5' />
            <circle cx='23' cy='5' r='5' />
            <circle cx='23' cy='23' r='5' />
            <circle cx='5' cy='23' r='5' />
          </svg>
        </div>

        <AnimatePresence>
          {active && (
            <m.div
              className={styles.button_burger_menu__svg}
              variants={anim_dots}
              initial={'initial'}
              animate={'animate'}
              exit={'exit'}
            >
              <m.svg viewBox='0 0 28 28' fill='white' xmlns='http://www.w3.org/2000/svg'>
                <circle cx='23' cy='5' r='5' />
                <circle cx='5' cy='23' r='5' />
                <path d='M9.93855 4.21382C9.56142 1.8259 7.49394 0 5 0C2.23858 0 0 2.23858 0 5C0 7.49394 1.8259 9.56142 4.21382 9.93855C11.3633 11.2167 16.7821 16.635 18.0611 23.7841C18.4374 26.173 20.5053 28 23 28C25.7614 28 28 25.7614 28 23C28 20.5053 26.173 18.4374 23.7841 18.0611C16.635 16.7821 11.2167 11.3633 9.93855 4.21382Z' />
              </m.svg>
            </m.div>
          )}
        </AnimatePresence>
      </div>
    </LazyMotion>
  )
}

export default memo(ButtonBurgerMenu)

// Component animate
const anim_dots = {
  initial: {
    opacity: 0,
  },
  animate: {
    opacity: 1,
    transition: { default: { duration: 0.25 } },
  },
  exit: {
    opacity: 0,
    transition: { default: { duration: 0.25 } },
  },
}
