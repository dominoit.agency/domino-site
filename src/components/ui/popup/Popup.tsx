// Libs
import React, { FC, ReactNode, memo, MouseEvent, CSSProperties } from 'react'
import styles from './popup.module.scss'
import { AnimatePresence, domAnimation, LazyMotion, m } from 'framer-motion'

// Interface
interface IPopup {
  active: boolean
  position?: 'relative' | 'absolute'
  positionX?: 'right' | 'left'
  items: {
    icon: ReactNode
    name: string
    onClick?: (e: MouseEvent) => void
  }[]
  style?: CSSProperties
}

// Component
const Popup: FC<IPopup> = ({
  active = false,
  items,
  position = 'absolute',
  positionX = 'left',
  style,
}) => {
  // Return
  return (
    <LazyMotion features={domAnimation}>
      <AnimatePresence>
        {active && (
          <m.div
            className={`${styles.popup_body}
            ${position === 'absolute' ? styles.absolute : ''}
            ${positionX === 'right' ? styles.right : ''}`}
            style={style}
            variants={anim_popup}
            initial={'initial'}
            animate={'animate'}
            exit={'exit'}
          >
            <m.div
              className={styles.popup_body__popup}
              variants={anim_popup_scroll}
              initial={'initial'}
              animate={'animate'}
              exit={'exit'}
            >
              <ul>
                {items &&
                  items.map((el, i) => (
                    <li
                      key={`SelectorPopup${el.name}${i}`}
                      onClick={(e) => el.onClick && el.onClick(e)}
                    >
                      <span>{el.icon}</span>
                      <span className={styles.text}>{el.name}</span>
                    </li>
                  ))}
              </ul>
            </m.div>
          </m.div>
        )}
      </AnimatePresence>
    </LazyMotion>
  )
}

export default memo(Popup)

// Component animate
const anim_popup = {
  initial: { height: '0' },
  animate: {
    height: 'auto',
    transition: {
      default: { duration: 0.25 },
    },
  },
  exit: {
    height: '0',
    transition: {
      default: { duration: 0.25 },
    },
  },
}
const anim_popup_scroll = {
  initial: { overflow: 'hidden' },
  animate: {
    overflow: 'auto',
    transition: {
      delay: 0.3,
    },
  },
  exit: { overflow: 'hidden' },
}
