import dynamic from 'next/dynamic'

export { default as Button } from './button/Button'
export { default as ButtonBurgerMenu } from './button-burger-menu/ButtonBurgerMenu'
export const ButtonScrollToTop = dynamic(() => import('./button-scroll-to-top/ButtonScrollToTop'), {
  ssr: false,
})
export { default as Input } from './input/Input'
export { default as Logo } from './logo/Logo'
export const ModalWindow = dynamic(() => import('./modal-window/ModalWindow'), {
  ssr: false,
})
export const Popup = dynamic(() => import('./popup/Popup'), {
  ssr: false,
})
export const ItemPreloader = dynamic(() => import('./preloader/item-preloader/ItemPreloader'), {
  ssr: false,
})
export const PagePreloader = dynamic(() => import('./preloader/page-preloader/PagePreloader'), {
  ssr: false,
})
export { default as Selector } from './selector/Selector'
