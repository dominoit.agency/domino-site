// Libs
import React, { CSSProperties, FC, memo, ReactNode } from 'react'
import styles from './button.module.scss'
import { AnimatePresence, domAnimation, LazyMotion, m } from 'framer-motion'

// Interface
interface IButton {
  svg?: ReactNode
  type?: 'button' | 'submit'
  size?: 'small' | 'large'
  variant?: 'primary' | 'secondary' | 'tertiary'
  children?: string
  onClick?: () => void
  loading?: boolean
  disable?: boolean
  onlyIco?: boolean
  style?: CSSProperties
}

// Component
const Button: FC<IButton> = ({
  svg,
  type = 'button',
  size = 'small',
  variant = 'primary',
  children,
  onClick,
  loading,
  disable,
  onlyIco = false,
  style,
}) => {
  // Return
  return (
    <LazyMotion features={domAnimation}>
      <button
        type={type}
        className={`${styles.button} 
          ${size === 'large' ? styles.large : ''}
          ${variant === 'secondary' ? styles.secondary : ''}
          ${variant === 'tertiary' ? styles.tertiary : ''}
          ${disable ? styles.disable : ''}
          ${loading ? styles.loading : ''}
          ${svg ? styles.svg : ''}
          ${!children && svg ? styles.only_svg : ''}`}
        onClick={onClick}
        style={style}
      >
        {svg ? (
          <div
            className={`${styles.button__svg} ${variant === 'secondary' ? styles.secondary : ''}`}
          >
            {!onlyIco ? (
              <>
                <div>{svg}</div>
                <div>{children}</div>
              </>
            ) : (
              svg
            )}
          </div>
        ) : (
          children
        )}

        <AnimatePresence>
          {loading && (
            <m.div
              className={styles.button__preloader}
              variants={anim_loader}
              initial={'initial'}
              animate={'animate'}
              exit={'exit'}
            >
              <div className={styles.button__preloader__loader} />
            </m.div>
          )}
        </AnimatePresence>
      </button>
    </LazyMotion>
  )
}

export default memo(Button)

// Component animate
const anim_loader = {
  initial: {
    opacity: 0,
  },
  animate: {
    opacity: 1,
    transition: { default: { duration: 0.25 } },
  },
  exit: {
    opacity: 0,
    transition: { default: { duration: 0.25 } },
  },
}
