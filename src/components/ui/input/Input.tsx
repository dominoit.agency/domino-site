// Libs
import React, { ChangeEvent, FC, forwardRef, memo, Ref, useState } from 'react'
import styles from './input.module.scss'
import { AnimatePresence, domAnimation, LazyMotion, m } from 'framer-motion'

// Interface
interface IInput {
  type?: 'text' | 'email' | 'password'
  title?: string
  name?: string
  defaultValue?: string
  value?: string
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void
  placeholder?: string
  readonly?: boolean
  errors?: string
  autoComplete?: 'on' | 'off'
}

// Component
const Input: FC<IInput> = forwardRef(
  (
    {
      type = 'text',
      title,
      value,
      name,
      defaultValue,
      onChange,
      placeholder,
      readonly = false,
      errors,
      autoComplete = 'on',
    },
    ref: Ref<HTMLInputElement>,
  ) => {
    // View password active
    const [passView, setPasView] = useState<boolean>(false)
    // Control password view
    const handlePassViewOnClick = () => setPasView(!passView)

    // Return
    return (
      <LazyMotion features={domAnimation}>
        <label>
          {title && <div className={styles.label}>{title}</div>}

          <div className={styles.wrapper}>
            <input
              ref={ref}
              name={name}
              style={{ paddingRight: type === 'password' ? '2.8rem' : '0' }}
              defaultValue={defaultValue}
              className={styles.wrapper__input}
              type={!passView ? type : 'text'}
              placeholder={placeholder}
              readOnly={readonly}
              autoComplete={autoComplete}
              value={value}
              onChange={onChange}
            />

            {type === 'password' &&
              (!passView ? (
                <div onClick={handlePassViewOnClick}>
                  <svg
                    className={styles.pass_view}
                    viewBox='0 0 18 18'
                    fill='none'
                    stroke='black'
                    xmlns='http://www.w3.org/2000/svg'
                  >
                    <path
                      d='M1.5 9C1.5 9 4.22727 3.75 9 3.75C13.7727 3.75 16.5 9 16.5 9C16.5 9 13.7727 14.25 9 14.25C4.22727 14.25 1.5 9 1.5 9Z'
                      strokeWidth='1.2'
                      strokeLinecap='round'
                      strokeLinejoin='round'
                    />
                    <path
                      d='M9 11.25C10.2426 11.25 11.25 10.2426 11.25 9C11.25 7.75736 10.2426 6.75 9 6.75C7.75736 6.75 6.75 7.75736 6.75 9C6.75 10.2426 7.75736 11.25 9 11.25Z'
                      strokeWidth='1.2'
                      strokeLinecap='round'
                      strokeLinejoin='round'
                    />
                  </svg>
                </div>
              ) : (
                <div onClick={handlePassViewOnClick}>
                  <svg
                    className={styles.pass_view}
                    stroke='black'
                    viewBox='0 0 28 28'
                    fill='none'
                    xmlns='http://www.w3.org/2000/svg'
                  >
                    <path
                      d='M11.3914 6.17772C12.2109 5.95845 13.0811 5.83333 14 5.83333C21.4243 5.83333 25.6667 14 25.6667 14C25.6667 14 24.6996 15.8617 22.8807 17.8074M5.07401 10.2413C3.28445 12.1692 2.33337 14 2.33337 14C2.33337 14 6.5758 22.1667 14 22.1667C14.9358 22.1667 15.821 22.0369 16.6535 21.8102M13.4167 17.4516C12.1518 17.2394 11.117 16.3488 10.6992 15.1667M14.5834 10.5484C16.049 10.7943 17.2058 11.951 17.4516 13.4167M3.50004 3.5L24.5 24.5'
                      strokeWidth='1.5'
                      strokeLinecap='round'
                      strokeLinejoin='round'
                    />
                  </svg>
                </div>
              ))}
          </div>

          <AnimatePresence>
            {errors && (
              <m.div variants={anim_error} initial={'initial'} animate={'animate'} exit={'exit'}>
                <p className={styles.error}>{errors}</p>
              </m.div>
            )}
          </AnimatePresence>
        </label>
      </LazyMotion>
    )
  },
)
Input.displayName = 'Input'
export default memo(Input)

// Component animate
const anim_error = {
  initial: {
    opacity: 0,
    height: '0',
  },
  animate: {
    opacity: 1,
    height: 'auto',
    transition: { default: { duration: 0.25 } },
  },
  exit: {
    opacity: 0,
    height: '0',
    transition: { default: { duration: 0.25 } },
  },
}
