// Libs
import React, { FC, memo, useEffect, useState } from 'react'
import styles from './button-scroll-to-top.module.scss'
import { AnimatePresence, domAnimation, LazyMotion, m } from 'framer-motion'
import { useRouter } from 'next/router'

// Component
const ButtonScrollToTop: FC = () => {
  // Router
  const { pathname } = useRouter()
  // Button active
  const [buttonActive, setButtonActive] = useState<boolean>(false)
  // Device type
  const [deviceType, setDeviceType] = useState<'desktop' | 'tablet' | 'mobile' | null>(null)
  // Go to top
  const handleGoToTop = () => {
    typeof window !== 'undefined' &&
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      })
  }
  // Device type
  const getDeviceType = () => {
    const ua = navigator.userAgent
    if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
      return 'tablet'
    }
    if (
      /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
        ua,
      )
    ) {
      return 'mobile'
    }
    return 'desktop'
  }
  // Watch from scroll
  useEffect(() => {
    // Header
    const header = document.querySelector('header')
    // Prev position
    let prevScrollPosition = window.scrollY
    // On scroll
    window.onscroll = () => {
      // Current position
      const currentScrollPosition = window.scrollY

      if (header) {
        // Button to top control
        if (
          prevScrollPosition > currentScrollPosition &&
          currentScrollPosition > header.clientHeight * 1.28
        ) {
          setButtonActive(true)
        } else setButtonActive(false)
        // Header control
        if (
          prevScrollPosition < currentScrollPosition &&
          currentScrollPosition >
            (getDeviceType() === 'desktop' ? header.clientHeight * 1.28 : header.clientHeight * 0.5)
        )
          header.style.top = `-${header.clientHeight}px`
        else header.style.top = `0`
      } else {
        // Button to top control
        if (prevScrollPosition > currentScrollPosition && currentScrollPosition > 10) {
          setButtonActive(true)
        } else setButtonActive(false)
      }

      // Return
      prevScrollPosition = currentScrollPosition
    }
  }, [pathname])
  // Watch from device type
  useEffect(() => {
    if (deviceType !== getDeviceType()) setDeviceType(getDeviceType())
  }, [])

  // Return
  return (
    <LazyMotion features={domAnimation}>
      <AnimatePresence>
        {buttonActive && (
          <m.div
            className={`${styles.button_scroll_tot_top} ${
              deviceType && deviceType !== 'desktop' ? styles.mobile_type : ''
            }`}
            onClick={handleGoToTop}
            variants={anim_visible}
            initial={'initial'}
            animate={'animate'}
            exit={'exit'}
          />
        )}
      </AnimatePresence>
    </LazyMotion>
  )
}

export default memo(ButtonScrollToTop)

// Component animate
const anim_visible = {
  initial: {
    opacity: 0,
    bottom: '-70px',
  },
  animate: {
    opacity: 1,
    bottom: '20px',
    transition: { default: { duration: 0.25, type: 'spring', stiffness: 100 } },
  },
  exit: {
    opacity: 0,
    bottom: '-70px',
    transition: { default: { duration: 0.25 } },
  },
}
