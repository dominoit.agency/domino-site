// Libs
import React, { FC, memo, useEffect, useState } from 'react'
import styles from './modal-window.module.scss'
import { AnimatePresence, domAnimation, LazyMotion, m } from 'framer-motion'
import Button from '../button/Button'

// Interface
interface IModalWindow {
  active: boolean
  header?: string
  content?: string
  textBtnCancel?: string
  onCancel?: () => void
  textBtnConfirm?: string
  onConfirm?: () => void
  classNameContent?: string
}

// Component
const ModalWindow: FC<IModalWindow> = ({
  active = false,
  header = 'Modal header',
  content = 'Modal content',
  textBtnCancel = 'Cancel',
  onCancel,
  textBtnConfirm = 'Confirm',
  onConfirm,
  classNameContent,
}) => {
  // Prev params
  const [prevParams, setPrevParams] = useState<string | null>(null)
  // Device type
  const getDeviceType = () => {
    const ua = navigator.userAgent
    if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua)) {
      return 'tablet'
    }
    if (
      /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(
        ua,
      )
    ) {
      return 'mobile'
    }
    return 'desktop'
  }
  // Watch from params
  useEffect(() => {
    const body = document.querySelector('html')

    !prevParams && body && setPrevParams(window.getComputedStyle(body).overflow)

    !active && setPrevParams(null)
  }, [active])
  // Control scroll
  useEffect(() => {
    const body = document.querySelector('html')

    if (body) {
      if (active) {
        body.style.overflow = 'hidden'
        if (getDeviceType() === 'desktop') body.style.paddingRight = '12px'
      } else if (!active && body) {
        body.style.overflow = prevParams || ''
        if (getDeviceType() === 'desktop') body.style.paddingRight = '0'
      }
    }
  }, [active])

  // Return
  return (
    <LazyMotion features={domAnimation}>
      <AnimatePresence>
        {active && (
          <m.div
            className={styles.modal_window}
            onClick={onCancel}
            variants={anim_modal_body}
            initial={'initial'}
            animate={'animate'}
            exit={anim_modal_body.exit(2)}
          >
            <m.div
              className={`${styles.modal_window__content} ${classNameContent || ''}`}
              onClick={(e) => e.stopPropagation()}
              variants={anim_modal_content}
              initial={'initial'}
              animate={anim_modal_content.animate(2)}
              exit={'exit'}
            >
              <div className={styles.modal_window__header}>{header}</div>

              <div className={styles.modal_window__description}>{content}</div>

              <div className={styles.modal_window__btns}>
                <Button onClick={onCancel} variant={'secondary'}>
                  {textBtnCancel}
                </Button>

                <Button onClick={onConfirm}>{textBtnConfirm}</Button>
              </div>
            </m.div>
          </m.div>
        )}
      </AnimatePresence>
    </LazyMotion>
  )
}

export default memo(ModalWindow)

// Component animate
const anim_modal_body = {
  initial: { y: '-120%', opacity: 0 },

  animate: (custom = 0) => ({
    y: '0',
    opacity: 1,
    transition: {
      delay: custom * 0.15,
      default: { duration: 0.25 },
    },
  }),

  exit: (custom = 0) => ({
    y: '-120%',
    opacity: 0,
    transition: {
      delay: custom * 0.15,
      default: { duration: 0.25 },
    },
  }),
}
const anim_modal_content = {
  initial: { height: '0' },

  animate: (custom = 0) => ({
    height: 'auto',
    transition: {
      delay: custom * 0.15,
      default: { duration: 0.25 },
    },
  }),

  exit: (custom = 0) => ({
    height: '0',
    transition: {
      delay: custom * 0.15,
      default: { duration: 0.25 },
    },
  }),
}
