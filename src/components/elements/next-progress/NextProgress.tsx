// Libs
import React, { FC, memo } from 'react'
import NextNProgress from 'nextjs-progressbar'

// Component
const NextProgress: FC = () => {
  // Return
  return (
    <NextNProgress
      nonce={''}
      color={'#0058DD'}
      startPosition={0.3}
      stopDelayMs={200}
      height={3}
      showOnShallow={true}
      options={{ easing: 'linear', speed: 250 }}
    />
  )
}

export default memo(NextProgress)
