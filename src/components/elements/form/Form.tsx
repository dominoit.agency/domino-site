// Libs
import React, { CSSProperties, FC, memo, ReactNode, useEffect } from 'react'
import {
  useForm,
  FormProvider,
  FieldValues,
  FieldErrors,
  UseFormRegister,
  useFormContext,
} from 'react-hook-form'

// Interface
interface IForm {
  onSubmit: (e: any) => void
  formMethods: (props: {
    isValid: boolean
    register: UseFormRegister<FieldValues>
    errors: FieldErrors
    getValues: (name: string) => string
    setValue: (
      name: string,
      value: string,
      options?:
        | Partial<{ shouldValidate: boolean; shouldDirty: boolean; shouldTouch: boolean }>
        | undefined,
    ) => void
  }) => ReactNode
  autoComplete?: 'on' | 'off'
  className?: string
  style?: CSSProperties
}

// Form component
const FormM: FC<IForm> = ({ autoComplete = 'on', className, style, onSubmit, formMethods }) => {
  // Services
  const { handleSubmit, formState, register, reset, clearErrors, setValue, getValues } =
    useFormContext()
  // Clear new form
  useEffect(() => {
    reset()
    clearErrors()
  }, [])

  // Return
  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
      className={className}
      style={style}
      autoComplete={autoComplete}
    >
      {formMethods({
        isValid: formState.isValid,
        register,
        errors: formState.errors,
        getValues,
        setValue,
      })}
    </form>
  )
}

export const Form = memo(FormM)

// Form provider
const FormSubmitProviderM: FC<{ children: ReactNode }> = ({ children }) => {
  // Methods
  const methods = useForm({
    mode: 'onChange',
  })

  // Return
  return <FormProvider {...methods}>{children}</FormProvider>
}

export const FormSubmitProvider = memo(FormSubmitProviderM)
