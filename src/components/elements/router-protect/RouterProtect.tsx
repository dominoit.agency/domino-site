// Libs
import { FC, memo, ReactNode, useEffect } from 'react'
import { useRouter } from 'next/router'

// Interface
export interface IUseRouterProtect {
  loading?: boolean
  privateRoutes: { routes: string[]; privateParams: boolean | undefined; redirect?: string }[]
  children: ReactNode
}

// Component
const RouterProtect: FC<IUseRouterProtect> = ({ loading = false, privateRoutes, children }) => {
  // Router
  const { pathname, push } = useRouter()
  // Watch from private router validation
  useEffect(() => {
    privateRoutes.map(
      (el) =>
        el.routes.filter((e) => pathname === e).length > 0 &&
        !el.privateParams &&
        !loading &&
        push(el.redirect ? el.redirect : '/').finally(),
    )
  }, [pathname, privateRoutes, loading])
  // Protect path
  if (
    !loading &&
    privateRoutes
      .map((el) => el.routes.filter((e) => pathname === e).length > 0 && !el.privateParams)
      .filter((val) => val).length > 0
  )
    return null

  // Public path
  return <>{children}</>
}

export default memo(RouterProtect)
