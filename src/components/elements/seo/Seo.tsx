// Libs
import { FC, memo } from 'react'
import Head from 'next/head'

// Interface
interface ISeo {
  title?: string
  locale?: string
  keywords?: string
  description?: string
  themeColor?: string
  backgroundColor?: string
  siteName?: string
  pageUrl?: string
  image?: string
  favicon?: string
}

// Component
const Seo: FC<ISeo> = ({
  title,
  locale = 'uk',
  keywords = 'Веб розробка',
  description = 'Розробка сайтів, дизайн сайтів',
  themeColor = '#242424',
  backgroundColor = '#242424',
  siteName = 'Coins office',
  image = '/seo-image.png',
  favicon = '/favicon.ico',
}) => {
  // Const
  const defaultTitle = 'Domino'

  // Return
  return (
    <Head>
      <title>{title && title !== 'undefined' ? `${title} / ${defaultTitle}` : defaultTitle}</title>

      <link rel='icon' href={favicon} />
      <meta name='robots' content='index,follow' />
      <meta name='viewport' content='width=device-width, initial-scale=1.0' />
      <meta name='display' content='standalone' />
      <meta charSet='utf-8' />
      <meta property='og:type' content='website' />

      <meta name='locale' content={locale} />
      <meta name='keywords' content={keywords} />
      <meta name='description' content={description} />
      <meta name='theme-color' content={themeColor} />
      <meta name='background-color' content={backgroundColor} />

      <meta property='og:site_name' content={siteName} />
      <meta property='og:title' content={title} />
      <meta property='og:locale' content={locale} />
      <meta property='og:description' content={description} />
      <meta property='og:image' content={`${process.env.SITE_URL}${image}`} />
    </Head>
  )
}

export default memo(Seo)
