// Libs
import React, {
  createContext,
  useContext,
  useState,
  ReactNode,
  FC,
  SetStateAction,
  Dispatch,
} from 'react'
import { initialState } from './initial-state'

// Interface
interface IAppContext {
  burgerMenu: boolean
  setBurgerMenu: Dispatch<SetStateAction<boolean>>
}

// Create context
// @ts-ignore
const AppContext = createContext<IAppContext>(null)

// Context provider
export const ContextProvider: FC<{ children: ReactNode }> = ({ children }) => {
  // Burger menu
  const [burgerMenu, setBurgerMenu] = useState<IAppContext['burgerMenu']>(initialState.burgerMenu)

  // Value
  const value = {
    burgerMenu,
    setBurgerMenu,
  }

  // Return
  return <AppContext.Provider value={value}>{children}</AppContext.Provider>
}

// Context hook
export const useAppContext = () => useContext(AppContext)
