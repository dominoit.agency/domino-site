interface IFields {
  title: string
  subtitle: string
  loginBtn: string
  registrationBtn: string
  googleBtn: string
  googleBtnLoading: string
  registrationLinkTitle: string
  registrationLink: string
  backBtn: string
  inputEmail: string
  inputPass: string
  inputConfirmPass: string
  inputName: string
  registrationTitle: string
  registrationSubtitle: string
}

interface IPage {
  [key: string]: IFields
}

export const authPage: IPage = {
  uk: {
    title: 'Ласкаво просимо',
    subtitle: 'Увійди до світу монет',
    loginBtn: 'Увійти',
    registrationBtn: 'Зареєструватись',
    googleBtn: 'Google вхід',
    googleBtnLoading: 'Вхід...',
    registrationLinkTitle: 'Ви ще не зареєстровані?',
    registrationLink: 'Зареєструватись.',
    backBtn: 'Назад',
    inputEmail: 'Емеіл',
    inputPass: 'Пароль',
    inputConfirmPass: 'Підтвердіть пароль',
    inputName: 'Нікнейм',
    registrationTitle: 'Реєстрація',
    registrationSubtitle: 'Створи свій coinsoffice',
  },

  en: {
    title: 'Welcome',
    subtitle: 'Enter the world of coin',
    loginBtn: 'Login',
    registrationBtn: 'Register',
    googleBtn: 'Google login',
    googleBtnLoading: 'Exit...',
    registrationLinkTitle: 'Not registered yet?',
    registrationLink: 'Register.',
    backBtn: 'Back',
    inputEmail: 'Email',
    inputPass: 'Password',
    inputConfirmPass: 'Confirm password',
    inputName: 'Nickname',
    registrationTitle: 'Registration',
    registrationSubtitle: 'Create your coinsoffice',
  },
}
