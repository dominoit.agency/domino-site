interface IFields {
  required: string
  emailValid: string
  emailNotAuth: string
  passwordWrong: string
  lengthPassword: string
  minLength2: string
  userFind: string
  passNotConfirm: string
  registrationOk: string
  loginOk: string
  logoutOk: string
  notAuth: string
}

interface IMessage {
  [key: string]: IFields
}

export const messages: IMessage = {
  uk: {
    required: "Обо'язкове поле",
    emailValid: 'Емеіл невірного формату',
    emailNotAuth: 'За даним емейлом користувача не існує',
    passwordWrong: 'Невірний пароль',
    lengthPassword: 'Мінімум 6 символів',
    minLength2: 'Мінімум 2 символа',
    userFind: 'Користувач з даним емейлом уже існує',
    passNotConfirm: 'Паролі не співпадають',
    registrationOk: 'Реєстрація пройшла успішно',
    loginOk: 'Вхід успішний',
    logoutOk: 'Вихід успішний',
    notAuth: 'Ви не авторизовані',
  },

  en: {
    required: 'Required',
    emailValid: 'Incorrect email format',
    emailNotAuth: 'There is no user for this email',
    passwordWrong: 'Invalid password',
    lengthPassword: 'Minimum 6 characters',
    minLength2: 'Minimum 2 characters',
    userFind: 'A user with this email address already exists',
    passNotConfirm: 'Passwords do not match',
    registrationOk: 'Registration was successful',
    loginOk: 'Login is successful',
    logoutOk: 'Exit is successful',
    notAuth: 'You are not authorized',
  },
}
