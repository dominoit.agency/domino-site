interface IFields {
  name: string
  href: string
}

interface IProfile {
  [key: string]: IFields[]
}

export const linksPublic: IProfile = {
  uk: [
    { name: 'Головна', href: '/' },
    { name: 'Каталог монет', href: '/catalog' },
    { name: 'Блог', href: '/blog' },
  ],

  en: [
    { name: 'Home', href: '/' },
    { name: 'Catalog of coins', href: '/catalog' },
    { name: 'Blog', href: '/blog' },
  ],
}
