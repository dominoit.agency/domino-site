interface IFields {
  login: string
  settings: string
  logout: string
  adminPanel?: string
}

interface IProfile {
  [key: string]: IFields
}

export const profile: IProfile = {
  uk: {
    login: 'Увійти',
    settings: 'Налаштування',
    logout: 'Вийти',
  },

  en: {
    login: 'Sign in',
    settings: 'Settings',
    logout: 'Logout',
  },
}

export const admin: IProfile = {
  uk: {
    login: 'Увійти',
    settings: 'Налаштування',
    adminPanel: 'Адмін панель',
    logout: 'Вийти',
  },

  en: {
    login: 'Sign in',
    settings: 'Settings',
    adminPanel: 'Admin panel',
    logout: 'Logout',
  },
}
