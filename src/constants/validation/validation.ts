// Email
export const emailPattern: { value: RegExp; message: string } = {
  value:
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  message: 'Емеіл невірного формату',
}

// Password
export const passwordPattern: { value: RegExp; message: string } = {
  value: /^[^ ]{6,}$/,
  message: 'Мінімум 6 символів, без пробілів',
}

// Phone
export const phoneNumberPattern: { value: RegExp; message: string } = {
  value: /^[0-9+ ]{6,}$/,
  message: "Обов'язкове поле",
}

export const minLength2Pattern: { value: RegExp; message: string } = {
  value: /^.{2,}$/,
  message: 'Мінімум 2 символів',
}
