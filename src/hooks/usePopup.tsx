// Libs
import { useEffect, useRef, useState } from 'react'

// Popup hook
export const usePopup = () => {
  // Ref
  const popupRef = useRef<HTMLInputElement | HTMLDivElement>(null)
  // Popup
  const [popup, setPopup] = useState(false)
  // Popup active
  const handlePopup = () => setPopup(!popup)
  // Outside click
  const handleOutPopupClick = (e: any) => {
    const path = e.path || (e.composedPath() && e.composedPath())
    if (!path.includes(popupRef.current)) setPopup(false)
  }
  // Watch from outside click
  useEffect(() => {
    popup && document.querySelector('html')?.addEventListener('click', handleOutPopupClick)
  }, [popup])

  // Return
  return { popup, handlePopup, popupRef }
}
