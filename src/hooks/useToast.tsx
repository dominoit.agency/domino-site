// Libs
import 'react-toastify/dist/ReactToastify.css'
import { toast } from 'react-toastify'
import { ToastContainer } from 'react-toastify'

// Interface
interface IToast {
  (msg: string, type: 'info' | 'success' | 'warning' | 'error'): void
}

// Toast hook
export const useToast = () => {
  // Toast
  const customToast: IToast = (msg, type) => {
    toast[type](msg, {
      position: 'bottom-left',
      autoClose: 2500,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      style: {
        background: 'rgba(0, 0, 0, 0.75)',
        backdropFilter: 'blur(20px)',
        fontFamily: 'Comfortaa, sans-serif',
        fontSize: '1.4rem',
        lineHeight: '1.2',
        color: `#fff`,
      },
    })
  }

  // Return
  return { customToast }
}

// Global element
export { ToastContainer }
