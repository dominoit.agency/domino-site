// Libs
import { useCallback, useLayoutEffect, useRef } from 'react'

// Event hook
export const useEvent = (handler: null) => {
  // Ref
  const handlerRef = useRef(null)
  // When render
  useLayoutEffect(() => {
    handlerRef.current = handler
  })

  // Return
  return useCallback((...args: any) => {
    // Handle ref
    const fn = handlerRef.current
    // eslint-disable-next-line @typescript-eslint/no-unused-vars,@typescript-eslint/ban-ts-comment
    // @ts-ignore
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    // Return
    return fn(...args)
  }, [])
}
